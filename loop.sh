#!/bin/bash
PLAYER="spotify"
INTERR=0

### DOCS
#	Variables
#		mpris_length
#		mpris_album
#		mpris_title
#		mpris_artist
#	Notes
#		To stop the loop, do INTERR=1
###

### User-declared function
# Man I fucking love golang
main() {
	# This is ran after the loop is called in the background

	# This waits for the event loop to die (which is never)
	# Basically, it blocks so the callback is always called
	wait 

	# This function exists so you could start a timer here 
	# and have callback reset/stop it, for time
}

callback() {
	for v in ${!mpris_*}; {
		echo "$v"'='"${!v}"
	}
}

### Do no touch
sleep_fn() {
	read -rst "${1:-1}" -N 999
}

### Event loop
evloop() {
	_plct=""
	for ((;;)); {
		[[ $INTERR -eq 1 ]] && exit
	
		plct=$(playerctl -p "$PLAYER" metadata)
		[[ "$_plct" = "$plct" ]] && {
			sleep_fn 0.5
			continue
		}
	
		_plct="$plct"
	
		IFS=$'\n' array=( ${plct//\, /$'\n'} )
		for l in "${array[@]}"; {
			case "$l" in
			"'mpris:length'"*)
				__a="${l##*uint64 }"
				mpris_length="${__a:0:-1}"
				printf -v "mpris_length" "%.0f" "${mpris_length:0:3}.${mpris_length:3}"
				;;
			"'xesam:album'"*)
				IFS="'" __a=( $l )
				mpris_album="${__a[3]}"
				;;
			"'xesam:title'"*)
				IFS="'" __a=( $l )
				mpris_title="${__a[3]}"
				;;
			"'xesam:albumArtist'"*)
				IFS="'" __a=( $l )
				mpris_artist="${__a[3]}"
				;;
			esac
		}
	
		callback
	}
}

evloop &
main